docker_down()
{
    cid=`docker ps | grep "$1" | awk '{print $1}'`
    [ "$cid" != "" ] && docker stop $cid
}

docker_clean()
{
    cid=`docker ps -a | grep "$1" | awk '{print $1}'`
    [ "$cid" != "" ] && docker rm $cid
}
