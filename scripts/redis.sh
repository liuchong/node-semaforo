#!/bin/bash

cd $(dirname $(readlink -f $0))
. ./funcs.sh

TMP_DIR=$PWD/tmp

up ()
{
    mkdir -p $TMP_DIR
    # start up master
    cp redis-master.conf $TMP_DIR/redis-master.conf && \
        chmod 666 $TMP_DIR/redis-master.conf && \
        docker run --name semaforo-redis-master --restart always -d \
               -p 6379:6379 \
               -v $TMP_DIR/redis-master.conf:/usr/local/etc/redis/redis.conf \
               redis redis-server /usr/local/etc/redis/redis.conf

    # start up slave
    cp redis-slave.conf $TMP_DIR/redis-slave.conf && \
        chmod 666 $TMP_DIR/redis-slave.conf && \
        docker run --name semaforo-redis-slave --restart always -d \
               --link semaforo-redis-master:redis-master-host \
               -p 6380:6379 \
               -v $TMP_DIR/redis-slave.conf:/usr/local/etc/redis/redis.conf \
               redis redis-server /usr/local/etc/redis/redis.conf

    # start up sentinels
    # 1
    cp sentinel-1.conf $TMP_DIR/sentinel-1.conf && \
        chmod 666 $TMP_DIR/sentinel-1.conf &&
        docker run --name semaforo-redis-sentinel-1 --restart always -d \
               --link semaforo-redis-master:redis-master-host \
               --link semaforo-redis-slave:redis-slave-host \
               -p 26379:26379 \
               -v $TMP_DIR/sentinel-1.conf:/usr/local/etc/redis/sentinel.conf \
               redis redis-server /usr/local/etc/redis/sentinel.conf --sentinel
    # 2
    cp sentinel-2.conf $TMP_DIR/sentinel-2.conf && \
        chmod 666 $TMP_DIR/sentinel-2.conf &&
        docker run --name semaforo-redis-sentinel-2 --restart always -d \
               --link semaforo-redis-master:redis-master-host \
               --link semaforo-redis-slave:redis-slave-host \
               -p 26380:26379 \
               -v $TMP_DIR/sentinel-2.conf:/usr/local/etc/redis/sentinel.conf \
               redis redis-server /usr/local/etc/redis/sentinel.conf --sentinel
    # 3
    cp sentinel-3.conf $TMP_DIR/sentinel-3.conf && \
        chmod 666 $TMP_DIR/sentinel-3.conf &&
        docker run --name semaforo-redis-sentinel-3 --restart always -d \
               --link semaforo-redis-master:redis-master-host \
               --link semaforo-redis-slave:redis-slave-host \
               -p 26381:26379 \
               -v $TMP_DIR/sentinel-3.conf:/usr/local/etc/redis/sentinel.conf \
               redis redis-server /usr/local/etc/redis/sentinel.conf --sentinel
}

down()
{
    docker_down semaforo-redis
}

clean()
{
    docker_clean semaforo-redis
}

eval $*
