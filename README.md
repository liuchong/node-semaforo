semaforo
=============

[![dependencies](https://david-dm.org/liuchong/node-semaforo.svg)](https://david-dm.org/liuchong/node-semaforo)
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fliuchong%2Fnode-semaforo.svg?type=shield)](https://app.fossa.io/projects/git%2Bgithub.com%2Fliuchong%2Fnode-semaforo?ref=badge_shield)
[![Build Status](https://api.travis-ci.org/liuchong/node-semaforo.svg?branch=master)](https://travis-ci.org/liuchong/node-semaforo)
[![codecov](https://codecov.io/gh/liuchong/node-semaforo/branch/master/graph/badge.svg)](https://codecov.io/gh/liuchong/node-semaforo)

[![styled with standard](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![npm version](https://img.shields.io/npm/v/semaforo.svg)](https://www.npmjs.com/package/semaforo)

A simple throttle program based on redis

***note: NEED feature "async/await" to run unit test***

## Usage

Please refer to [init.test.js](./lib/init.test.js) and [helper/redis.test.js](./lib/helper/redis.test.js).

## License

[MIT](LICENSE)

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fliuchong%2Fnode-semaforo.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2Fliuchong%2Fnode-semaforo?ref=badge_large)
